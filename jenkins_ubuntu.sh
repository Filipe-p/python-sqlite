# Pre-requisist
sudo apt install ca-certificates
sudo apt install openjdk-11-jre-headless -y

# Adding to source list, updating and installing
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'
sudo apt-get update
sudo apt-get install jenkins -y

# Starting jenkins
sudo systemctl start jenkins