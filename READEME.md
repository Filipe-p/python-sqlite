# Python SQL connection 

In this repo we connect to our SQL server from our machines using python. 

We'll use the `pgdb` or `pg` external module.

### Pre Requisits 

You will need:

- postgress sql server running
- it's address (local or web)
- If needed, in AWS, apropriate networking & SG to allow communications

### Learning outcomes

This exercise covers:

- Git, bitbucket MD
- Python
- SQL
- (possibly) OOP
- Seperation of concern
- (possibly) Flask

### Task 

We're going to continue the Ebook.you project from exercises 101-103:

https://bitbucket.org/Filipe-p/sql_basics_exercises/src/master/Exercises/

We're starting by doing a simple python connection. Objective is to do function for CRUD for: 

- Users
- Books
- Sales


#### Extra

In no perticular order, try to tacle of of these. 

Make a OOP software: 

- Make it into OOP
  - Make a class conneciton to DB 
  - Make subclassess for each table (users, books, Sales) that inherit connections

Make an API:

- Make it into an API with Flask
  - use youtube

Make a website:

- Make it into a website with Flask 
  - use youtube 



#### pip3 install pgdb



#### slqlite command

Createing DB 

```bash
sqlite3 database_name.db
```


Create Tables 

```sql 
CREATE TABLE users(
  user_id INTEGER PRIMARY KEY ASC, 
  f_name VARCHAR(40), 
  l_name VARCHAR(40), 
  email VARCHAR(40));
```

books

```sql 
CREATE TABLE books(
  books_id INTEGER PRIMARY KEY ASC, 
  user_id INTEGER,
  title VARCHAR(40), 
  content TEXT,
  FOREIGN KEY(user_id) REFERENCES users(user_id));
```

Sales

```sql 
CREATE TABLE sales(
  sales_id INTEGER PRIMARY KEY ASC, 
  user_id INTEGER,
  book_id, INTEGER,
  FOREIGN KEY(user_id) REFERENCES users(user_id),
  FOREIGN KEY(book_id) REFERENCES books(book_id));
```


Creating Data

```SQL

INSERT INTO users(f_name, l_name , email)
VALUES 
   ("Nando", "Oliveira" , "nando@nandos.co.uk"),
   ("Fernando", "Silva" , "silva@nandos.co.uk"),
   ("Maria", "Cruz" , "cruz@nandos.co.uk");


INSERT INTO books(user_id, title , content)
VALUES 
   ("1", "Peri Peri Chicken" , "Lemon, herbs, piri and tiny chickens"),
   ("1", "Gambas e Vinho" , "prawns, limon, herbs, garlic, white wine and some flames. "),
   ("2", "Travelling and getting lost" , "no better way to get lost than  to jump on ship, work cleaning the floors and hope you survive to next port.");

INSERT INTO sales(user_id, book_id)
VALUES 
   (1, 2),
   (1, 3),
   (3, 1);

```

Getting Data